#! /bin/bash

count=0

for host in `seq 1 254`
do
   host_ip=192.168.1.$host
   result=`ping -c 1 $host_ip | grep "bytes from"`
   if [ "$result" != "" ]
   then
	echo -n $host_ip
	echo -e "\t\t\033[32mOK!\033[0m"
	let "count=count+1"
   fi
done

echo  Total -  $count 

