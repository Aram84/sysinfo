#!/bin/bash

#Contains function names
FUNCTIONS=("cpu" "ram" "hdd" "video" "net" "system")

#Flags for function calls
FLAGS=(0 0 0 0 0 0)

#CPU information
function cpu {
echo -e "\n\033[1;32;41m********************** CPU ***********************\033[0m"
echo -e -n "\033[1;34mProcessors\033[0m\t"
echo -e ": `grep -c 'processor' /proc/cpuinfo`"
echo -e -n "\033[1;34mModel name\033[0m\t"
echo -e "`grep -m 1 'model name' /proc/cpuinfo | cut -f2`"
echo -e -n "\033[1;34mVendor ID\033[0m\t"
echo -e "`grep -m 1 'vendor_id' /proc/cpuinfo | cut -f2`"
echo -e -n "\033[1;34mCPU family\033[0m\t"
echo -e "`grep -m 1 'cpu family' /proc/cpuinfo | cut -f2`"
echo -e -n "\033[1;34mModel\033[0m\t\t"
echo -e "`grep -m 1 'model' /proc/cpuinfo | cut -f3`"
echo -e -n "\033[1;34mFPU\033[0m\t\t"
echo -e "`grep -m 1 'fpu' /proc/cpuinfo | cut -f3`"
}

#RAM information
function ram {
echo -e "\033[1;32;41m********************** RAM ***********************\033[0m"
echo -e "\033[32mTotal (GB)\tUsed (GB)\tFree (GB)"
echo -e "==========\t=========\t=========\033[0m"
#Second string of command "free"
TMP=`free | sed -n 2p`
#Total memory
TOTAL_KB=`echo $TMP | cut -d ' ' -f2`
TOTAL_GB=$(echo "scale=2; $TOTAL_KB/1024^2" | bc)
echo -n -e "$TOTAL_GB\t\t"
#Used memory
USED_KB=`echo $TMP | cut -d ' ' -f3`
USED_GB=$(echo "scale=2; $USED_KB/1024^2" | bc)
echo -n -e "$USED_GB\t\t"
#Free memory
echo "$(echo "scale=2; $TOTAL_GB-$USED_GB" | bc)"
}

#HDD information
function hdd {
echo -e "\033[1;32;41m********************** HDD ***********************\033[0m"
echo -e "\033[32mName\tType\tSize"
echo -e "=====\t=====\t======\033[0m"
echo -e "`lsblk -r -o "NAME","TYPE","SIZE" | tr ' ' '\t' | grep "sd[a-z]*"`"
}


#Information about video
function video {
echo -e "\033[1;32;41m********************** VIDEO *********************\033[0m"
echo -e -n "\033[1;34mVGA Controller\033[0m\t"
echo  "  :   `lspci | grep VGA | cut -d ' ' -f5-`"
echo -e -n "\033[1;34mCurrent resolution\033[0m"
echo ": `xrandr -q | grep '\*'`"

}

#Network information
function net {
echo -e "\033[1;32;41m********************** Network *******************\033[0m"
echo -e "\033[32mHardware\tStatus\tTotal"
echo -e "========\t======\t=====\033[0m"
#Lan 
echo -e -n "\033[1;34mLAN\033[0m\t\t"
if [ `ifconfig | grep -c "eth"` -gt 0 ]
then
	echo -e "yes\t`ifconfig | grep -c 'eth'`"
else
	echo -e "no\t-"
fi
#WiFi information
echo -e -n "\033[1;34mWiFi\033[0m\t\t"
if [ `ifconfig | grep -c "wlan"` -gt 0 ]
then
	echo -e "yes\t`ifconfig | grep -c 'wlan'`"
else
	echo -e "no\t-"
fi
}

#Information about the system
function system {
echo -e "\033[1;32;41m********************** System ********************\033[0m"
echo -e "\033[1;34mName:\033[0m\t`uname -a | cut -d ' ' -f1,4,12,15`"
echo -e "\033[1;34mKernel:\033[0m\t`uname -a | cut -d ' ' -f3`\n"
}

#Print help information about the script
function print_help {
echo -e "\n\033[31mUsage:\033[0m ./getinfo.sh OPTION[s]"
echo "List information about your computer hardware."
echo -e "Option(s) can be set width spaces or without (-c -r -d  or -crd)."
echo -e "\033[32mOPTIONS\033[0m"
echo "	--help	show help"
echo "	--all	show all information about your computer"
echo "	-c	information about the computer's processor"
echo "	-r	information about the computer's RAM"
echo "	-d	information about the computer's HDD(s)"
echo "	-v	information about the display resolution and graphics card"
echo "	-n	information about the computer's Network adapters"
echo -e "	-s	information about the computer's OS\n"
}


#Calls functions for which
#the flag is set to 1
function print_info {
for INDEX in `seq 0 5`
do
	if [ ${FLAGS[$INDEX]} -gt 0 ]
	then
		${FUNCTIONS[$INDEX]}
	fi
done
}


#Call without options or
#with options more than five
if [ $# -lt 1 -o $# -gt 6 ]
then 
	echo -e "Usage: info1.sh OPTION\nTry 'info1.sh --help' for more information."
	exit 1
fi


#Call with one option
if [ $# -eq 1 ]
then 
	if [ $1 == "--help" ]
	then 
		print_help
		exit 0
	elif [ $1 == "--all" ]
	then
		for INDEX in `seq 0 5`
		do
			${FUNCTIONS[$INDEX]}
		done
		exit 0
	fi
fi

while getopts "crdvns" OPTION
do
	case $OPTION in
		c) 	
		if [ ${FLAGS[0]} -eq 0 ]
		then
			FLAGS[0]=1
		else
			echo -e -n "\033[31mRecurring option -c\033[0m: "
			echo "Try './getinfo.sh --help' for more information."
			exit 1
		fi
		;;
		r) 	
		if [ ${FLAGS[1]} -eq 0 ]
		then
			FLAGS[1]=1
		else
			echo -e -n "\033[31mRecurring option -r\033[0m: "
			echo "Try './getinfo.sh --help' for more information."
			exit 1
		fi
		;;
		d)
		if [ ${FLAGS[2]} -eq 0 ]
		then
			FLAGS[2]=1
		else
			echo -e -n "\033[31mRecurring option -d\033[0m:"
			echo "Try './getinfo.sh --help' for more information. "
			exit 1
		fi
		;;
		v)
		if [ ${FLAGS[3]} -eq 0 ]
		then
			FLAGS[3]=1
		else
			echo -e -n "\033[31mRecurring option -d\033[0m:"
			echo "Try './getinfo.sh --help' for more information. "
			exit 1
		fi
		;;
		n)
		if [ ${FLAGS[4]} -eq 0 ]
		then
			FLAGS[4]=1
		else
			echo -e -n "\033[31mRecurring option -n\033[0m:"
			echo "Try './getinfo.sh --help' for more information. "
			exit 1
		fi
		;;
		s)
		if [ ${FLAGS[5]} -eq 0 ]
		then
			FLAGS[5]=1
		else
			echo -e -n "\033[31mRecurring option -s\033[0m: "
			echo "Try './getinfo.sh --help' for more information."
			exit 1
		fi
		;;
		*)
		echo "Try './getinfo.sh --help' for more information"
		exit 1
		;;
	esac
done

print_info
echo ""

